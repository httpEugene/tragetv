Bitbucket: https://bitbucket.org/JohnnyDes/tragetv/src
TDD - Karma
CI - MagnumCI
Code check - ESlint rules
BDD - jasmine
MagnumCI intagration with Slack chat

To run unit tests:
	gulp test

To run code check:
	gulp lint

To run build:
	gulp

After each push on MagnumCI build triggers and build result sending to slack chat.